<?php

$file = 'data.csv';

$dictionary = array();
$handle = fopen($file, "r");
$no = 1;
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        if (trim($line) == "") // skip if code is empty
            continue;

        //$l = explode(";", trim($line));

        $item = new stdClass();
        $item->text = trim(substr($line, 0, strpos($line, ";")));
        $item->meaning = trim(substr($line, strpos($line, ";") + 1));
        $dictionary[] = $item;
		json_encode($item);
		if(json_last_error() == JSON_ERROR_UTF8)
		{
			echo($no);
			exit;
		}
		$no++;
    }
    fclose($handle);
}

$result = json_encode($dictionary);
file_put_contents("data.json", $result);
?>