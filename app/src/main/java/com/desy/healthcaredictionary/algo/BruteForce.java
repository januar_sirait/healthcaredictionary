package com.desy.healthcaredictionary.algo;

import com.desy.healthcaredictionary.adapter.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Januar-PC on 3/13/2017.
 */

public class BruteForce {
    public static boolean match(String pattern, String subject){
        int n = subject.length();
        int m = pattern.length();

        char sub[] = subject.toCharArray();
        char patt[] = pattern.toCharArray();

        for (int i = 0; i < n-m+1; i++){
            int j = 0;
            while (j < m && sub[i+j] == patt[j]){
                j++;
            }
            if (j == m)
                return true; //return i;
        }

        return false; // return -1;
    }

    public static List<DictionaryItem> match(String pattern, List<DictionaryItem> data){
        List<DictionaryItem> result = new ArrayList<DictionaryItem>();
        for (DictionaryItem item : data) {
            if (match(pattern, item.getText()))
                result.add(item);
        }
        return result;
    }
}
