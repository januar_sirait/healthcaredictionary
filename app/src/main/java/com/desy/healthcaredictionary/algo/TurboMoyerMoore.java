package com.desy.healthcaredictionary.algo;

import com.desy.healthcaredictionary.adapter.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Januar-PC on 5/17/2017.
 */

public class TurboMoyerMoore {
    private int m;
    private int[] bmGs;
    private int[] bmBc;
    private char[] x;

    private static void preBmBc(char[] x, int[] bmBc)
    {
        int m = x.length;

        for (int i = 0; i < bmBc.length; i++)
            bmBc[i] = m;
        for (int i = 0; i < m - 1; i++)
            bmBc[x[i]] = (m - i - 1);
    }

    private static void suffixes(char[] x, int[] suff) {
        int f = 0;
        int m = x.length;

        suff[(m - 1)] = m;
        int g = m - 1;
        for (int i = m - 2; i >= 0; i--)
            if ((i > g) && (suff[(i + m - 1 - f)] < i - g)) {
                suff[i] = suff[(i + m - 1 - f)];
            } else {
                if (i < g)
                    g = i;
                f = i;
                while ((g >= 0) && (x[g] == x[(g + m - 1 - f)]))
                    g--;
                suff[i] = (f - g);
            }
    }

    private static void preBmGs(char[] x, int[] bmGs)
    {
        int m = x.length;
        int[] suff = new int[m];

        suffixes(x, suff);

        for (int i = 0; i < m; i++)
            bmGs[i] = m;
        int j = 0;
        for (int i = m - 1; i >= 0; i--)
            if (suff[i] == i + 1)
                for (; j < m - 1 - i; j++)
                    if (bmGs[j] == m)
                        bmGs[j] = (m - 1 - i);
        for (int i = 0; i <= m - 2; i++)
            bmGs[(m - 1 - suff[i])] = (m - 1 - i);
    }

    public static List search(String pattern, String source) {
        char[] x = pattern.toCharArray(); char[] y = source.toCharArray();
        int m = x.length; int n = y.length;
        List result = new ArrayList();

        int[] bmGs = new int[m];
        int[] bmBc = new int[255];

        preBmGs(x, bmGs);
        preBmBc(x, bmBc);

        // searching
        int u;
        int j = u = 0;
        int shift = m;
        while (j <= n - m) {
            int i = m - 1;
            while ((i >= 0) && (x[i] == y[(i + j)])) {
                i--;
                if ((u != 0) && (i == m - 1 - shift))
                    i -= u;
            }
            if (i < 0) {
                result.add(Integer.valueOf(j));
                shift = bmGs[0];
                u = m - shift;
            } else {
                int v = m - 1 - i;
                int turboShift = u - v;
                int bcShift = bmBc[y[(i + j)]] - m + 1 + i;
                shift = Math.max(turboShift, bcShift);
                shift = Math.max(shift, bmGs[i]);
                if (shift == bmGs[i]) {
                    u = Math.min(m - shift, v);
                } else {
                    if (turboShift < bcShift)
                        shift = Math.max(shift, u + 1);
                    u = 0;
                }
            }
            j += shift;
        }

        return result;
    }

    public static List<DictionaryItem> search(String pattern, List<DictionaryItem> data){
        List<DictionaryItem> result = new ArrayList<DictionaryItem>();
        for (DictionaryItem item : data) {
            if (search(pattern, item.getText()).size() > 0)
                result.add(item);
        }
        return result;
    }
}
