package com.desy.healthcaredictionary.algo;

import com.desy.healthcaredictionary.adapter.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Januar-PC on 5/17/2017.
 */

public class NotSoNaive {

    public static boolean search(String pattern, String source){
        int j, k, ell;
        char[] x = pattern.toCharArray();
        char[] y = source.toCharArray();
        int m = x.length;
        int n = y.length;

        /* preprocessing */
        if (x[0] == x[1]){
            k = 2;
            ell = 1;
        }else{
            k = 1;
            ell = 2;
        }

        /* searching */
        j = 0;
        while (j <= n-m){
            if (x[1] != y[j + 1]){
                j +=k;
            }else{
                if (memcmp(x, y, 2, m-2, j) == 0 && x[0] == y[j]){
                    return true;
                }
                j += ell;
            }
        }

        return false;
    }

    public static int memcmp(char[] a, char[] b,
                             int offset, int length, int offset2) {
        if (a == b && a != null) {
            return 0;
        }
        length += offset;
        for (int i = offset; i < length; i++) {
            if (a[i] != b[i+offset2]) {
                return (a[i] & 0xFF) - (b[i] & 0xFF);  // "promote" to unsigned.
            }
        }
        return 0;
    }

    public static List<DictionaryItem> search(String pattern, List<DictionaryItem> data){
        List<DictionaryItem> result = new ArrayList<DictionaryItem>();
        for (DictionaryItem item : data) {
            if (search(pattern, item.getText()))
                result.add(item);
        }
        return result;
    }
}
