package com.desy.healthcaredictionary.algo;

import com.desy.healthcaredictionary.adapter.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Januar-PC on 3/13/2017.
 */

public class Horspool {
    private int SIZE = 256;
    private int table[];

    private Horspool() {
        table = new int[SIZE];
    }

    public static Horspool with() {
        return new Horspool();
    }

    public Horspool shifttable(String pattern) {

        int m = pattern.length();
        char p[] = pattern.toCharArray();

        for (int i = 0; i < SIZE; i++)
            table[i] = m;
        for (int j = 0; j < m - 1; j++)
            table[p[j]] = m - 1 - j;

        return this;
    }

    public int match(String pattern, String source) {
        char s[] = source.toCharArray();
        char p[] = pattern.toCharArray();
        int m = pattern.length();
        int n = source.length();

        int i, k;
        i = m - 1;
        while (i <= n - 1) {
            k = 0;
            while (k <= m - 1 && p[m - 1 - k] == s[i - k]) {
                k++;
            }

            if (k == m) {
                return i - m + 1;
            } else {
                i = i + table[s[i]];
            }
        }
        return -1;
    }

    public List<DictionaryItem> match(String pattern, List<DictionaryItem> data) {
        List<DictionaryItem> result = new ArrayList<DictionaryItem>();
        for (DictionaryItem item : data) {
            if (match(pattern, item.getText()) > -1)
                result.add(item);
        }
        return result;
    }
}
