package com.desy.healthcaredictionary.algo;

import com.desy.healthcaredictionary.adapter.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Januar on 04/06/2017.
 */

public class ZhuTakaoka {

    private static int ASIZE = 255;

    private static void suffixes(char[] x, int[] suff) {
        int f = 0;
        int m = x.length;

        suff[(m - 1)] = m;
        int g = m - 1;
        for (int i = m - 2; i >= 0; i--)
            if ((i > g) && (suff[(i + m - 1 - f)] < i - g)) {
                suff[i] = suff[(i + m - 1 - f)];
            } else {
                if (i < g)
                    g = i;
                f = i;
                while ((g >= 0) && (x[g] == x[(g + m - 1 - f)]))
                    g--;
                suff[i] = (f - g);
            }
    }

    private static void preBmGs(char[] x, int[] bmGs)
    {
        int m = x.length;
        int[] suff = new int[m];

        suffixes(x, suff);

        for (int i = 0; i < m; i++)
            bmGs[i] = m;
        int j = 0;
        for (int i = m - 1; i >= 0; i--)
            if (suff[i] == i + 1)
                for (; j < m - 1 - i; j++)
                    if (bmGs[j] == m)
                        bmGs[j] = (m - 1 - i);
        for (int i = 0; i <= m - 2; i++)
            bmGs[(m - 1 - suff[i])] = (m - 1 - i);
    }

    private static void preZtBc(char[] x, int[][] ztBc){
        for (int i =0; i < ASIZE; ++i)
            for (int j = 0; j < ASIZE; j++)
                ztBc[i][j] = x.length;
        for (int i =0; i < ASIZE; ++i)
            ztBc[i][x[0]] = x.length - 1;
        for (int i = 1; i < x.length - 1; ++i)
            ztBc[x[i - 1]][x[i]] = x.length - 1 - i;
    }

    public static boolean search(String pattern, String source){
        char[] x = pattern.toCharArray(); char[] y = source.toCharArray();
        int m = x.length; int n = y.length;
        int i, j;
        int[][] ztBc = new int[ASIZE][ASIZE];
        int[] bmGs = new int[m];
        List result = new ArrayList();

        /* Preprocessing */
        preZtBc(x, ztBc);
        preBmGs(x, bmGs);

        /* Searching */
        j = 0;
        while (j <= n - m) {
            i = m - 1;
            try {
                while (i < m && x[i] == y[i + j])
                    --i;
            }catch (IndexOutOfBoundsException e){}
            if (i < 0) {
                j += bmGs[0];
                return true;
            }
            else
                j += Math.max(bmGs[i],
                        ztBc[y[j + m - 2]][y[j + m - 1]]);
        }

        return false;
    }

    public static List<DictionaryItem> search(String pattern, List<DictionaryItem> data){
        List<DictionaryItem> result = new ArrayList<DictionaryItem>();
        for (DictionaryItem item : data) {
            if (search(pattern, item.getText()))
                result.add(item);
        }
        return result;
    }
}
