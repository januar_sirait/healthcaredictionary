package com.desy.healthcaredictionary.helper;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Januar-PC on 3/13/2017.
 */

public class AssetsHelper {
    private Context context;

    private AssetsHelper(Context context){
        this.context = context;
    }

    public static AssetsHelper with(Context c){
        return new AssetsHelper(c);
    }

    public String getString(String file){
        String json = null;
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
