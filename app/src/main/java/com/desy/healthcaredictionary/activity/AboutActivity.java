package com.desy.healthcaredictionary.activity;

import android.os.Bundle;

import com.desy.healthcaredictionary.R;
import com.desy.healthcaredictionary.core.ParentActivity;

public class AboutActivity extends ParentActivity {

    public AboutActivity() {
        super(R.layout.activity_about, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
