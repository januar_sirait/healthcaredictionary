package com.desy.healthcaredictionary.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.desy.healthcaredictionary.R;
import com.desy.healthcaredictionary.core.ParentActivity;
import com.desy.healthcaredictionary.fragment.ZhuTakaokaFragment;
import com.desy.healthcaredictionary.fragment.KnuthMorrisPrattFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ParentActivity {

    private static final String TAG = "MainActivity";
    private CardView card_zhu;
    private CardView card_knuth;

    public MainActivity() {
        super(R.layout.activity_main);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        card_zhu = (CardView)findViewById(R.id.card_zhu);
        card_knuth = (CardView)findViewById(R.id.card_knuth);

        card_zhu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ZhuTakaokaActivity.class);
                startActivity(intent);
            }
        });

        card_knuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, KnuthMorrisPrattActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
