package com.desy.healthcaredictionary.activity;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.desy.healthcaredictionary.R;
import com.desy.healthcaredictionary.adapter.DictionaryItem;
import com.desy.healthcaredictionary.adapter.ResultAdapter;
import com.desy.healthcaredictionary.algo.KnuthMorrisPratt;
import com.desy.healthcaredictionary.core.ParentActivity;
import com.desy.healthcaredictionary.helper.AssetsHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class KnuthMorrisPrattActivity extends ParentActivity {
    private ListView listView;
    private ResultAdapter adapter;
    private List<DictionaryItem> dictionaryItemList;
    private List<DictionaryItem> dataSource;

    private EditText txt_cari;
    private ImageView btn_cari;
    private TextView lbl_running_time;
    private TextView lbl_not_found;

    public KnuthMorrisPrattActivity() {
        super(R.layout.activity_knuth_morris_pratt, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initElement();
        dictionaryItemList = new ArrayList<DictionaryItem>();
        dataSource = new ArrayList<DictionaryItem>();

        Type listType = new TypeToken<ArrayList<DictionaryItem>>(){}.getType();
        dataSource = new Gson().fromJson(AssetsHelper.with(this).getString("data.json"), listType);

        adapter = new ResultAdapter(this, R.layout.result_item, dictionaryItemList);
        listView = (ListView)findViewById(R.id.list_result);
        listView.setAdapter(adapter);
    }

    private void initElement(){
        txt_cari = (EditText)findViewById(R.id.txt_cari);
        btn_cari = (ImageView)findViewById(R.id.btn_cari);
        lbl_running_time = (TextView)findViewById(R.id.txt_running_time);
        lbl_not_found = (TextView)findViewById(R.id.lbl_not_found);

        btn_cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(txt_cari.getText().toString())){
                    long startTime = System.nanoTime();
                    String pattern = txt_cari.getText().toString().toLowerCase();
                    dictionaryItemList = KnuthMorrisPratt.search(pattern, dataSource);
                    long endTime = System.nanoTime();

                    adapter.setData(dictionaryItemList);
                    if (dictionaryItemList.size() <= 0)
                        lbl_not_found.setVisibility(View.VISIBLE);
                    else
                        lbl_not_found.setVisibility(View.GONE);

                    lbl_running_time.setText("Running Time : " + ((float)(endTime - startTime) / 1000000) + " ms");
                }else{
                    adapter.setData(new ArrayList<DictionaryItem>());
                    lbl_running_time.setText("");
                }
            }
        });

        txt_cari.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN){
                    if (keyCode == KeyEvent.KEYCODE_ENTER){
                        btn_cari.performClick();
                        return true;
                    }
                }
                return false;
            }
        });
    }
}
