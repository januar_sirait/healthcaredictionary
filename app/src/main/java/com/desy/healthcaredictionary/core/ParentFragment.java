package com.desy.healthcaredictionary.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Januar on 10/03/2017.
 */

public class ParentFragment<T> extends Fragment {
    protected View view;
    protected AppCompatActivity activity;
    protected Bundle args;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (savedInstanceState == null) {
            args = new Bundle();
        }else{
            args = savedInstanceState;
        }
        return view;
    }

    protected View findViewById(int id) {
        return view.findViewById(id);
    }

    protected FragmentManager getSupportFragmentManager(){
        if (this.activity != null)
        {
            return this.activity.getSupportFragmentManager();
        }

        return null;
    }

    public T classHelper(){
        return (T)this;
    }
}
