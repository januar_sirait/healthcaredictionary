package com.desy.healthcaredictionary.core;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.desy.healthcaredictionary.R;

/**
 * Created by Januar on 10/03/2017.
 */

public class ParentActivity extends AppCompatActivity {
    protected Toolbar toolbar;
    protected Boolean enableBackButton;
    protected int resLayoutId;
    protected int resToolbarColor;

    public ParentActivity(int resLayoutId){
        this.enableBackButton = false;
        this.resLayoutId = resLayoutId;
    }

    public ParentActivity(int resLayoutId, Boolean enableBackButton){
        this.enableBackButton = enableBackButton;
        this.resLayoutId = resLayoutId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(this.resLayoutId);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        if (toolbar != null){
            toolbar.setFitsSystemWindows(true);
            setSupportActionBar(toolbar);
            if (this.enableBackButton) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }
}
