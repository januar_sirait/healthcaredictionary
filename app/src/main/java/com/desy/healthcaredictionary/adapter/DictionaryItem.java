package com.desy.healthcaredictionary.adapter;

/**
 * Created by Januar on 11/03/2017.
 */

public class DictionaryItem {
    private String text;
    private String meaning;

    public DictionaryItem(){
        this.text = "";
        this.meaning = "";
    }

    public DictionaryItem(String text, String meaning)
    {
        this.text = text;
        this.meaning = meaning;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getMeaning() {
        return meaning;
    }
}
