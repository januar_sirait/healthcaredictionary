package com.desy.healthcaredictionary.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.desy.healthcaredictionary.R;

import java.util.List;

/**
 * Created by Januar on 11/03/2017.
 */

public class ResultAdapter extends ArrayAdapter<DictionaryItem> {
    private Context context;
    private List<DictionaryItem> dictionaryItems;
    private int resource;
    private View view;

    public ResultAdapter(Context context, int resource, List<DictionaryItem> dictionaryItems) {
        super(context, resource, dictionaryItems);
        this.context = context;
        this.dictionaryItems = dictionaryItems;
        this.resource = resource;
    }

    public void setData(List<DictionaryItem> data){
        dictionaryItems.clear();
        dictionaryItems = data;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dictionaryItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemHolder holder;
        view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            holder = new ItemHolder();
            view = inflater.inflate(resource, parent, false);
            holder.text = (TextView)view.findViewById(R.id.txt_text);
            holder.meaning = (TextView)view.findViewById(R.id.txt_meaning);
            view.setTag(holder);
        }else{
            holder = (ItemHolder) view.getTag();
        }

        DictionaryItem item = dictionaryItems.get(position);
        holder.text.setText(item.getText());
        holder.meaning.setText(item.getMeaning());

        return view;
    }

    class ItemHolder{
        TextView text;
        TextView meaning;
    }
}
